// this components is not stable and should not be  use in prod
import PropTypes from 'prop-types'
import { TextField } from '@mui/material'

export function Input({ name, formik, ...props }) {
    return (
        <TextField
            {...props}
            name={name}
            value={formik.values[name]}
            onChange={formik.handleChange}
            helperText={formik.touched[name] && formik.errors[name]}
            error={formik.touched[name] && Boolean(formik.errors[name])}
        />
    )
}

Input.propTypes = {
    name: PropTypes.string.isRequired,
    formik: PropTypes.object.isRequired,
}

Input.defaultProps = {
    hiddenLabel: true,
    fullWidth: true,
    margin: 'normal',
    // variant: 'standard',
}