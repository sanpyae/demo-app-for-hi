import { Fragment, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { AvatarWrapper, Menu, Note } from './styles'
// redux
import { removeAuth } from 'redux/auth/authSlice'
import { useDispatch, useSelector } from 'react-redux'
// mui
import AccountBoxOutlinedIcon from '@mui/icons-material/AccountBoxOutlined'
import HelpOutlineIcon from '@mui/icons-material/HelpOutline'
import ManageAccountsOutlinedIcon from '@mui/icons-material/ManageAccountsOutlined'
import LogoutOutlinedIcon from '@mui/icons-material/LogoutOutlined'
import { Avatar, Box, Divider, IconButton, ListItemIcon, MenuItem, Typography } from '@mui/material'

export default function ProfileMenu() {
    const [anchorEl, setAnchorEl] = useState(null)
    const navigate = useNavigate()
    const dispatch = useDispatch()
    const auth = useSelector((state) => state.auth)

    const handleOpen = (e) => setAnchorEl(e.currentTarget)

    const handleClose = () => setAnchorEl(null)

    const handleClick = (e) => {
        // code
    }

    const logout = () => {
        setAnchorEl(null)
        dispatch(removeAuth())
        navigate('/auth/login')
    }

    return (
        <Fragment>
            <IconButton sx={{ p: 0 }} onClick={handleOpen}>
                <Avatar alt={auth.username} src='https://mui.com/static/images/avatar/2.jpg' />
            </IconButton>
            <Menu
                onClose={handleClose}
                onClick={handleClick}
                anchorEl={anchorEl}
                open={Boolean(anchorEl)}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                }}
                transformOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}>
                <AvatarWrapper>
                    <Avatar alt={auth.username} src='https://mui.com/static/images/avatar/2.jpg' />
                    <Box>
                        <Typography variant='subtitle2' >
                            {auth.username}
                        </Typography>
                        <Note variant='subtitle2'>
                            {auth.email}
                        </Note>
                    </Box>
                </AvatarWrapper>
                <Divider sx={{ mb: 1 }} />
                <MenuItem onClick={handleClose}>
                    <ListItemIcon>
                        <AccountBoxOutlinedIcon fontSize='small' />
                    </ListItemIcon>
                    Profile
                </MenuItem>
                <MenuItem onClick={handleClose}>
                    <ListItemIcon>
                        <ManageAccountsOutlinedIcon fontSize='small' />
                    </ListItemIcon>
                    Account
                </MenuItem>
                <MenuItem onClick={handleClose}>
                    <ListItemIcon>
                        <HelpOutlineIcon fontSize='small' />
                    </ListItemIcon>
                    Help
                </MenuItem>
                <Divider />
                <MenuItem onClick={logout}>
                    <ListItemIcon>
                        <LogoutOutlinedIcon fontSize='small' />
                    </ListItemIcon>
                    Logout
                </MenuItem>
            </Menu>
        </Fragment>
    )
}