import { useNavigate } from 'react-router-dom'
// mui
import { styled } from '@mui/material/styles'
import { Card, CardActions, CardContent, CardMedia, Typography } from '@mui/material'

const CardWrapper = styled(Card)(({ theme }) => ({
}))

const Thumbnail = styled(CardMedia)(({ theme }) => ({
}))

const Price = styled(Typography)(({ theme }) => ({
    fontSize: 30,
    fontWeight: 500,
}))

export function ProductItem({ item }) {
    const navigate = useNavigate()

    const handleClick = () => {
        navigate(`/products/${item.id}`, { state: { item } })
    }

    return (
        <CardWrapper elevation={2} onClick={handleClick}>
            <Thumbnail
                component='img'
                src={item.image}
            />
            <CardContent>
                <Typography component='div' variant='h5'>
                    {item.name}
                </Typography>
                <Typography paragraph>
                    {item.description}
                </Typography>
            </CardContent>

            <CardActions disableSpacing>
                <Price color='primary.main'>
                    {item.amount}
                </Price>
            </CardActions>
        </CardWrapper>
    )
}