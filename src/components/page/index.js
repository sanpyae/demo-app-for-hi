import { useEffect } from 'react'
import config from 'common/config'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { Helmet } from 'react-helmet-async'
// redux
import { useDispatch } from 'react-redux'
import { setTitle } from 'redux/app/appSlice'
// mui
import { Box, Breadcrumbs, Hidden, Link as MuiLink, Typography } from '@mui/material'

function Breadcrumb({ children, to }) {
    if (!to) return <Typography variant='body2' color='text.secondary'>{children}</Typography>

    return (
        <MuiLink
            to={to}
            color='inherit'
            underline='none'
            component={Link}
            sx={{ display: 'flex', alignItems: 'center' }}
        >
            <Typography variant='body2' color='text.primary'>{children}</Typography>
        </MuiLink>
    )
}

export function Title({ title }) {
    return (
        <Helmet>
            <title>{title || config.site.name}</title>
        </Helmet>
    )
}

function Page({ breadcrumbs, children, title, ...restProps }) {
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(setTitle(title))
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <Box {...restProps}>
            <Helmet>
                <title>{title || config.site.name}</title>
            </Helmet>
            <Hidden smDown>
                <Box sx={{ mb: 3 }}>
                    <Box sx={{ flexGrow: 1 }}>
                        {title && <Typography variant='h4'>{title}</Typography>}
                        {breadcrumbs && (
                            <Breadcrumbs separator='›' aria-label='breadcrumb'>
                                <Breadcrumb to='/'>Home</Breadcrumb>
                                {breadcrumbs.map((v, k) => (
                                    <Breadcrumb key={k} to={v.to}>{v.label}</Breadcrumb>
                                ))}
                            </Breadcrumbs>
                        )}
                    </Box>
                </Box>
            </Hidden>
            {children}
        </Box>
    )
}

Page.propTypes = {
    title: PropTypes.string,
    children: PropTypes.node.isRequired,
}

export default Page