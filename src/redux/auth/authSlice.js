import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'

import api from 'common/api'
import { endpoint } from 'common/endpoint'

const initialState = {
    token: '',
    username: '',
}

export const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        setAuth: (state, action) => {
            const { token, username } = action.payload
            return {
                token: token,
                username: username,
                email: `${username}@host.com`,
            }
        },
        removeAuth:  (state) => {
            return initialState
        },
    },
    extraReducers(builder) {
        builder.addCase(login.fulfilled, (state, action) => {
            // We can directly add the new post object to our posts array
            const { token, username } = action.payload
            state.auth = {
                token: token,
                username: username,
                email: `${username}@host.com`,
            }
        })
    },
})

export const login = createAsyncThunk(
    'auth/login',
    async initUsr => {
        const response = await api().post(endpoint.register, initUsr)

        const fakeData = {
            ...response.data,
            auth: true,
            username: initUsr.username,
            email: `${initUsr.username}@host.com`,

        }
        return fakeData
    }
)

export const { setAuth, removeAuth } = authSlice.actions

export default authSlice.reducer