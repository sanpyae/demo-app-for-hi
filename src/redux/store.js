import throttle from 'lodash.throttle'
import { configureStore } from '@reduxjs/toolkit'
import { loadState, saveState } from 'common/localStorage'
// reducer
import appReducer from './app/appSlice'
import authReducer from './auth/authSlice'
import themeReducer from './theme/themeSlice'

export const store = configureStore({
	reducer: {
		app: appReducer,
		auth: authReducer,
		theme: themeReducer,

	},
	preloadedState: loadState(),
})

store.subscribe(throttle(() => {
	saveState({
		auth: store.getState().auth,
		theme: store.getState().theme,
	})
}, 1000))