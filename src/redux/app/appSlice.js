import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    title: 'Home',
}

export const appSlice = createSlice({
    name: 'app',
    initialState,
    reducers: {
        setTitle: (state, action) => {
            state.title = action.payload
        },
    },
})

// Action creators are generated for each case reducer function
export const { setTitle } = appSlice.actions

export default appSlice.reducer