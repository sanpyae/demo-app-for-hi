import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined'
import CreditCardOutlinedIcon from '@mui/icons-material/CreditCardOutlined'
import NotificationsOutlinedIcon from '@mui/icons-material/NotificationsOutlined'
import PaletteOutlinedIcon from '@mui/icons-material/PaletteOutlined'
import SecurityOutlinedIcon from '@mui/icons-material/SecurityOutlined'
import SettingsOutlinedIcon from '@mui/icons-material/SettingsOutlined'

const menus = [
    {
        to: '/setting/profile',
        label: 'Profile',
        icon: <AccountCircleOutlinedIcon/>
    },
    {
        to: '/setting/account',
        label: 'Account',
        icon: <SettingsOutlinedIcon/>
    },
    {
        to: '/setting/security',
        label: 'Security',
        icon: <SecurityOutlinedIcon/>
    },
    {
        to: '/setting/noti',
        label: 'Notification',
        icon: <NotificationsOutlinedIcon/>
    },
    {
        to: '/setting/billing',
        label: 'Billing',
        icon: <CreditCardOutlinedIcon/>
    },
    {
        to: '/setting/theme',
        label: 'Theme',
        icon: <PaletteOutlinedIcon/>
    },
]

export default menus