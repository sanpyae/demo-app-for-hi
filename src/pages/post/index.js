import Page from 'components/page'

const breadcrumbs = [{ label: 'Post' }]

export function PostPage() {
    return (
        <Page title='Post' breadcrumbs={breadcrumbs}>
            Content
        </Page>
    )
}