import api from 'common/api'
import { endpoint } from 'common/endpoint'

export function login(val) {
    return api().post(endpoint.login, val).then(req => req.data)
}

export function register(val) {
    return api().post(endpoint.register, val).then(req => req.data)
}
