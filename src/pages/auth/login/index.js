import { Title } from 'components/page'
import { useSnackbar } from 'notistack'
import { Link, useNavigate } from 'react-router-dom'
// action
import { login } from '../action'
// redux
import { useDispatch } from 'react-redux'
import { setAuth } from 'redux/auth/authSlice'
// form
import * as yup from 'yup'
import { useFormik } from 'formik'
import { Input } from 'components/form'
// mui
import { styled } from '@mui/material/styles'
import { Box, Button, Checkbox, FormControlLabel, Typography } from '@mui/material'

const Content = styled(Box)(({ theme }) => ({
    height: '80vh',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    padding: theme.spacing(0, 5)
}))

const Form = styled('form')(({ theme }) => ({
    [theme.breakpoints.up('md')]: {
        width: 500
    },
}))

const Label = styled(Typography)(({ theme }) => ({
    ...theme.typography.h4,
    fontFamily: 'JosefinSans, sans-serif',
}))

const Action = styled(Box)(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between'
}))

const TextWrapper = styled(Box)(({ theme }) => ({
    fontWeight: 500,
    textAlign: 'center',
    color: theme.palette.text.secondary
}))

const LinkText = styled(Link)(({ theme }) => ({
    fontWeight: 500,
    textDecoration: 'none',
    color: theme.palette.primary.main
}))

const validationSchema = yup.object({
    username: yup
        .string('Enter your username')
        .required('Username is required'),
    password: yup
        .string('Enter your password')
        .min(8, 'Password should be of minimum 8 characters length')
        .required('Password is required'),
})

export function LoginPage() {
    const navigate = useNavigate()
    const dispatch = useDispatch()
    const { enqueueSnackbar } = useSnackbar()
    const formik = useFormik({
        initialValues: {
            username: '',
            password: '',
        },
        validationSchema: validationSchema,
        onSubmit: async (values) => {
            const data = await login(values)
            if(!data.token) {
                enqueueSnackbar('Invalid username or password', { variant: 'error' })
            } else {
                dispatch(setAuth({
                    token: data.token,
                    username: values.username,
                }))
                navigate('/')
            }
        },
    })  

    return (
        <Box>
            <Title title='Login' />
            <Content>
                <Box
                    sx={{
                        marginTop: 8,
                        display: 'flex',
                        flexDirection: 'column',
                    }}
                >
                    <Label>Hello! Welcome back</Label>
                    <Form onSubmit={formik.handleSubmit}>
                        <Input
                            formik={formik}
                            name='username'
                            label='Username'
                            autoComplete='username'
                            autoFocus
                        />
                        <Input
                            formik={formik}
                            name='password'
                            label='Password'
                            type='password'
                            autoComplete='current-password'
                        />
                        <Action>
                            <FormControlLabel
                                control={<Checkbox value='remember' color='primary' />}
                                label='Remember me'
                            />
                            <LinkText to='/auth/login' variant='body2'>
                                forgot password?
                            </LinkText>
                        </Action>
                        <Button
                            type='submit'
                            fullWidth
                            variant='contained'
                            sx={{ mt: 3, mb: 2 }}
                        >
                            Sign In
                        </Button>
                        <TextWrapper>
                            Don't have an account? {' '}
                            <LinkText to='/auth/register' variant='body2'>
                                Sign Up Now
                            </LinkText>
                        </TextWrapper>
                    </Form>
                </Box>
            </Content>
        </Box>
    )
}