import { useFetch } from './action'
// comp
import Page from 'components/page'
import { ProductItem } from 'components/product'
// mui
import { Grid } from '@mui/material'

const breadcrumbs = [{ label: 'Shop' }]

export function List() {
    const [data, loading] = useFetch()

    return (
        <Page title='Shop' breadcrumbs={breadcrumbs}>
            {loading ? 'loading' :
                <Grid container spacing={4}>
                    {data?.content.map(item => (
                        <Grid key={item.id} item sm={6} md={4}>
                            <ProductItem item={item} />
                        </Grid>
                    ))}
                </Grid>
            }
        </Page>
    )
}