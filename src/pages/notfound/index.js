import { useNavigate } from 'react-router-dom'
import svg from 'assets/images/illustrations/page_not_found.svg'
// mui
import { styled } from '@mui/material/styles'
import { Box, Button, Stack, Typography } from '@mui/material'


const Center = styled(Box)(() => ({
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
}))

const Content = styled(Stack)(() => ({
    display: 'flex',
    alignItems: 'center',
}))

const Img = styled('img')(({ theme }) => ({
    width: 300,
    marginTop: 100,
    textAlign: 'center',
    [theme.breakpoints.up('sm')]: {
        width: 400,
        marginTop: 200,
    },
}))

const Title = styled(Typography)(({ theme }) => ({
    ...theme.typography.h3,
    marginTop: 30,
    fontWeight: 500,
    color: theme.palette.warning.main
}))

const Note = styled(Typography)(({ theme }) => ({
    ...theme.typography.body1,
    marginTop: 5,
    color: theme.palette.text.secondary
}))

const GoBack = styled(Button)(({ theme }) => ({
    borderRadius: 100
}))

export function NotfoundPage() {
    const navigate = useNavigate()

    const goBack = () => navigate(-1)

    return (
        <Center>
            <Content>
                <Img src={svg} alt='404' />
                <Title>Oops!</Title>
                <Note >
                    We can't seem to find the page you are looking for
                </Note>
                <GoBack variant='contained' onClick={goBack} sx={{ mt: 5, px: 10 }}>
                    Go Back
                </GoBack>
            </Content>
        </Center>
    )
}