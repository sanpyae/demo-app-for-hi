import Page from 'components/page'

const breadcrumbs = [{ label: 'Post' }]

export function PagePage() {
    return (
        <Page title='Page' breadcrumbs={breadcrumbs}>
            Content
        </Page>
    )
}