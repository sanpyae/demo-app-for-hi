import AuthLayout from 'layouts/auth'
import DashboardLayout from 'layouts/dashboard'
// auth page
import * as auth from 'pages/auth'
// public page
import { HomePage } from 'pages/home'
import { PagePage } from 'pages/page'
import { PostPage } from 'pages/post'
import { NotfoundPage } from 'pages/notfound'
import * as shop from 'pages/shop'
import * as setting from 'pages/setting'

const routes = [
    {
        path: '/',
        children: [
            {
                path: '/auth',
                element: <AuthLayout />,
                children: [
                    {
                        path: 'login',
                        element: <auth.LoginPage />
                    },
                    {
                        path: 'register',
                        element: <auth.RegisterPage />,
                    },
                ]
            },
            {
                path: '/',
                element: <DashboardLayout />,
                children: [
                    {
                        index: true,
                        element: <HomePage />
                    },
                    {
                        path: 'page',
                        element: <PagePage />,
                    },
                    {
                        path: 'post',
                        element: <PostPage />,
                    },
                    {
                        path: 'shop',
                        element: <shop.List />,
                    },
                    {
                        path: 'products/:id',
                        element: <shop.Detail />,
                    },
                    {
                        path: 'setting',
                        element: <setting.Layout />,
                        children: [
                            {
                                index: true,
                                element: 'setting'
                            },
                            {
                                path: 'account',
                                element: <setting.AccountPage />
                            },
                            {
                                path: 'billing',
                                element: <setting.BillingPage />
                            },
                            {
                                path: 'noti',
                                element: <setting.NotiPage />
                            },
                            {
                                path: 'profile',
                                element: <setting.ProfilePage />
                            },
                            {
                                path: 'security',
                                element: <setting.SecurityPage />
                            },
                            {
                                path: 'theme',
                                element: <setting.ThemePage />
                            },
                            {
                                path: '*',
                                element: <NotfoundPage />,
                            }
                        ]
                    },
                    {
                        path: '*',
                        element: <NotfoundPage />,
                    },
                ]
            }
        ]
    }
]

export default routes