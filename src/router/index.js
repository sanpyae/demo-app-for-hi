import routes from './config'
import { useRoutes } from 'react-router-dom'

function RouterConfig() {
    let element = useRoutes(routes)

    return element
}


export default RouterConfig