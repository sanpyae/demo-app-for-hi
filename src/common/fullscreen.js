import fscreen from 'fscreen'
import { useCallback, useEffect, useMemo, useState } from 'react'

export function useFullScreen(el) {
    const [active, setActive] = useState(false)

    useEffect(() => {
        const handleChange = () => {
            setActive(Boolean(fscreen.fullscreenElement))
        }
        fscreen.addEventListener('fullscreenchange', handleChange)
        return () => fscreen.removeEventListener('fullscreenchange', handleChange)
    }, [el])


    const enter = useCallback(() => {
        if (fscreen.fullscreenElement) {
            return fscreen.exitFullscreen().then(() => {
                return fscreen.requestFullscreen(el)
            })
        } else {
            return fscreen.requestFullscreen(el)
        }
    }, [el])

    const exit = useCallback(() => {
        if (fscreen.fullscreenElement) {
            return fscreen.exitFullscreen()
        }
        return Promise.resolve()
    }, [])

    return useMemo(
        () => ({
            active,
            enter,
            exit,
        }),
        [active, enter, exit],
    )
}