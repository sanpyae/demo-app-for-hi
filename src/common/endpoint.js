export const url = 'https://assessment-api.hivestage.com/api/'
export const endpoint = {
    login: 'auth/login',
    register: 'auth/register',
    
    order: 'orders',
    product: 'products',
}
