// localStorage.js
import CryptoJS from 'crypto-js'

const STORAGE_KEY = 'prl'
const SECRET_KEY = 'encryption-key-for-preload-state'

export function encrypt(data, secret) {
	const ciphertext = CryptoJS.AES.encrypt(JSON.stringify(data), secret)
	return ciphertext.toString()
}

export function decrypt(data, secret) {
	var bytes = CryptoJS.AES.decrypt(data, secret)
	return JSON.parse(bytes.toString(CryptoJS.enc.Utf8))
}

export const loadState = () => {
	try {
		const store = localStorage.getItem(STORAGE_KEY)
		const state = decrypt(store, SECRET_KEY)
		return JSON.parse(state)
	} catch {
		// ignore write errors
	}
}

export const saveState = (state) => {
	try {
		const serializedState = encrypt(JSON.stringify(state), SECRET_KEY)
		localStorage.setItem(STORAGE_KEY, serializedState)
	} catch {
		// ignore write errors
	}
}

export const clearState = () => {
	try {
		localStorage.removeItem(STORAGE_KEY)
	} catch {
		// ignore write errors
	}
}
