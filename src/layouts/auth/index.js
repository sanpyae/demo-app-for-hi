import { useIsAuth } from 'common/hook'
import { Outlet } from 'react-router-dom'
// mui
import { styled } from '@mui/material/styles'
import { Box, CssBaseline, Grid, Typography } from '@mui/material'
// res
import svg from 'assets/images/illustrations/web_shop.svg'

const Root = styled(Grid)(() => ({
    width: '100%',
    height: '100vh',
}))

const Right = styled(Grid)(({ theme }) => ({
    display: 'none',
    [theme.breakpoints.up('sm')]: {
        height: '100vh',
        display: 'flex',
        textAlign: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        background: theme.palette.primary.main,
    },
}))

const Img = styled('img')(({ theme }) => ({
    width: 200,
    marginBottom: '15vh',
    [theme.breakpoints.up('md')]: {
        width: 400,
    },
}))

const Note = styled(Typography)(({ theme }) => ({
    ...theme.typography.h5,
    marginTop: 30,
    fontWeight: 700,
    fontSize: '2.25rem',
    marginBottom: 30,
    fontFamily: 'JosefinSans, sans-serif',
    color: theme.palette.mode === 'dark' ? theme.palette.common.black : theme.palette.common.white,
}))

export default function Comp() {
    useIsAuth()
    
    return (
        <Root container component='main'>
            <CssBaseline />
            <Right item xs={false} sm={6} md={5}>
                <Box>
                    <Note >Welcome to our shopping site</Note>
                    <Img src={svg} alt='404' />
                </Box>
            </Right>
            <Grid item xs={12} sm={6} md={7}>
                <Outlet />
            </Grid>
        </Root>
    )
}