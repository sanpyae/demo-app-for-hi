// redux
import { store } from 'redux/store'
import { Provider } from 'react-redux'
import { useSelector } from 'react-redux'
// router
import RouterConfig from 'router'
import { BrowserRouter } from 'react-router-dom'
// theme
import { getTheme } from 'common/theme'
import { Box } from '@mui/material'
import { ThemeProvider, styled } from '@mui/material/styles'
// helmet
import { HelmetProvider } from 'react-helmet-async'
import { SnackbarProvider } from 'notistack'

function App() {
    return (
        <HelmetProvider>
            <Provider store={store}>
                <MuiProvider >
                    <SnackbarProvider anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                    }}>
                        <BrowserRouter>
                            <RouterConfig />
                        </BrowserRouter>
                    </SnackbarProvider>
                </MuiProvider>
            </Provider>
        </HelmetProvider>
    )
}

const Layout = styled(Box)(({ theme, mode }) => ({
    background: theme.palette.background.default
}))

function MuiProvider({ children }) {
    const state = useSelector((state) => state.theme)
    const theme = getTheme(state)

    return (
        <ThemeProvider theme={theme} >
            <Layout id='main-layout'>
                {children}
            </Layout>
        </ThemeProvider>
    )
}

export default App