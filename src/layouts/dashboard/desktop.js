import { cloneElement } from 'react'
import { useSelector } from 'react-redux'
import { Link, useMatch } from 'react-router-dom'
// assets
import logo from 'assets/images/logo/128.png'
// self
import menus from './menu'
import Profile from 'components/profile'
import { AvatarWrapper, Drawer, Logo, List, ListItem } from './comp'
// mui
import { Box, Container, CssBaseline, Divider, ListItemIcon, ListItemText, Tooltip } from '@mui/material'

function NavItem({ item }) {
    let match = useMatch(item.to)
    const icon = cloneElement(item.icon, { sx: { fontSize: 22 } })

    return (
        <Tooltip arrow title={item.label} placement='right'>
            <ListItem button active={match} to={item.to} component={Link}>
                <ListItemIcon>
                    {icon}
                </ListItemIcon>
                <ListItemText primary={item.label} />
            </ListItem>
        </Tooltip>
    )
}

export default function Dashboard(props) {
    const theme = useSelector((state) => state.theme)

    return (
        <Box sx={{ display: 'flex' }}>
            <CssBaseline enableColorScheme />
            <Drawer variant='permanent'>
                <Logo >
                    <img src={logo} alt='logo' />
                </Logo>
                <List>
                    {menus.map((item, index) => (
                        <NavItem key={index} item={item} />
                    ))}
                </List>
                <Box sx={{ flexGrow: 1 }} />
                <Divider />
                <AvatarWrapper>
                    <Profile />
                </AvatarWrapper>
            </Drawer>
            <Container maxWidth={theme.stretch ? false : 'lg'} sx={{ p: 0 }}>
                <Box component='main' sx={{ flexGrow: 1, py: 2 }}>
                    {props.children}
                </Box>
            </Container>
        </Box>
    )
}