import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
// redux
import { useSelector } from 'react-redux'
// self
import menus from './menu'
import ProfileMenu from 'components/profile'
// mui
import MenuIcon from '@mui/icons-material/Menu'
import { alpha, styled } from '@mui/material/styles'

import { AppBar, Avatar, Box, CssBaseline, IconButton, List, ListItem, ListItemIcon, ListItemText, Stack, SwipeableDrawer, Toolbar, Typography } from '@mui/material'


const DRAWER_WIDTH = 280
const APPBAR_HEIGHT = 64

const StyledAppbar = styled(AppBar)(({ theme }) => ({
    // boxShadow: 'none',
    backdropFilter: 'blur(6px)',
    WebkitBackdropFilter: 'blur(6px)',
    backgroundColor: alpha(theme.palette.background.default, 0.72),
}))

const StyledToolbar = styled(Toolbar)(({ theme }) => ({
    minHeight: APPBAR_HEIGHT,
}))

const DrawerHeader = styled(Toolbar)(({ theme }) => ({
    position: 'relative',
    display: 'flex',
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
}))

const Note = styled('span')(({ theme }) => ({
    ...theme.typography.body2,
    margin: 0,
    color: theme.palette.text.secondary
}))

const Title = styled(Typography)(({ theme }) => ({
    ...theme.typography.h6,
    color: theme.palette.text.secondary
}))

function DrawerContent({ onClose }) {
    let navigate = useNavigate()

    const handleClick = (to) => () => {
        onClose()
        navigate(to)
    }

    return (
        <Box>
            <DrawerHeader>
                <Stack spacing={1}>
                    <Box sx={{ m: 'auto', mt: 4 }}>
                        <Avatar alt='Username' src='https://mui.com/static/images/avatar/2.jpg' sx={{ width: 80, height: 80 }} />
                    </Box>
                    <Stack >
                        <Typography variant='subtitle2'>
                            San Pyae Lin
                        </Typography>
                        <Note variant='subtitle2'>
                            sanpyaelin.dev@gmail.com
                        </Note>
                    </Stack>
                </Stack>
            </DrawerHeader>
            <List>
                {menus.map((v, i) => (
                    <ListItem button key={i} onClick={handleClick(v.to)}>
                        <ListItemIcon>{v.icon}</ListItemIcon>
                        <ListItemText primary={v.label} />
                    </ListItem>
                ))}
            </List>
        </Box>
    )
}

export default function Layout(props) {
    const [open, setOpen] = useState(false)
    const app = useSelector((state) => state.app)

    const handleOpen = () => setOpen(true)
    const handleClose = () => setOpen(false)

    return (
        <Box >
            <CssBaseline enableColorScheme />
            <StyledAppbar position='fixed'>
                <StyledToolbar >
                    <IconButton aria-label='open drawer' sx={{ mr: 2 }} onClick={handleOpen}>
                        <MenuIcon />
                    </IconButton>
                    <Title variant='h6' sx={{ flexGrow: 1 }}>{app.title}</Title>
                    <ProfileMenu />
                </StyledToolbar>
            </StyledAppbar>
            <SwipeableDrawer
                open={open}
                onOpen={handleOpen}
                onClose={handleClose}
                PaperProps={{
                    sx: { width: DRAWER_WIDTH }
                }}
            >
                <DrawerContent onClose={handleClose} />
            </SwipeableDrawer>
            <Box component='main' sx={{ width: '100%' }}>
                <StyledToolbar />
                <Box component='main' sx={{ px: 1, py: 3 }}>
                    {props.children}
                </Box>
            </Box>
        </Box>
    )
}