This is a Mui Simple Layout project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
    yarn start
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

## Structure
```
└── src
    ├── assets
    │   ├── css
    │   ├── images    
    │   └── other     (other resource files video, audio or etc)
    ├── common
    ├── components
    ├── layout
    │   ├── app       (root layout)
    │   ├── auth      (remove if u don't need auth layout such as sign in, sign up, forgot pass)
    │   └── dashboard (dashboard layout)
    ├── pages
    │   ├── home      
    │   ├── crud      
    │   │   ├── list      
    │   │   ├── form   
    │   │   │   ├── create  
    │   │   │   └── edit  
    │   │   └── detail 
    │   └── settings 
    ├── redus
    │   ├── app       (app config)
    │   ├── theme     (theme setting)
    │   └── etc     
    └── router
```

## Deploy on GitHud Page

```bash
    yarn build
```

And then

```bash
    yarn deploy
```



## Tech Stack
- React Router v6
- Redux ToolKit
- Mui v5 (using styled or emotion instead of jss)
